FROM ubuntu

ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && apt-get install -y libgl-dev libglib2.0-dev python3-dev python3-pip git

WORKDIR /workspace

COPY ./requirements.txt .
RUN python3 -m pip install -r requirements.txt

COPY . .
RUN ./build.sh

RUN git clone --depth=1 https://github.com/cleardusk/3DDFA_V2 /tmp/original
RUN cp -r /tmp/original/weights/* /workspace/weights/
RUN cp -r /tmp/original/configs/* /workspace/configs/

EXPOSE 5000
CMD python3 app.py